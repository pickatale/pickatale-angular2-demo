System.register(['angular2/core', 'angular2/router', 'angular2/http', 'rxjs/Rx'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_1, http_1;
    var BookComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            BookComponent = (function () {
                function BookComponent(_http, routeParams) {
                    this._http = _http;
                    this.routeParams = routeParams;
                    this.version = '1.0';
                    this.id = '305';
                    this.lang = 'EN';
                    this.config = {
                        version: this.version,
                        id: this.id,
                        lang: this.lang,
                        baseUrl: 'http://cdn.pickatale.com/book_generated/',
                        url: function () {
                            return this.baseUrl + this.id + '_' + this.version + '/' + this.id + '_' + this.lang + '_' + this.version + '/book-data.json';
                        },
                        coverUrl: function () {
                            return this.baseUrl + this.id + '_' + this.version + '/' + this.id + '_' + this.lang + '_' + this.version + '/';
                        }
                    };
                }
                BookComponent.prototype.ngOnInit = function () {
                    var _this = this;
                    this._http.get(this.config.url())
                        .map(function (res) { return res.json(); })
                        .subscribe(function (data) { return _this.initBook(data); }, function (err) { return _this.handleError(err); });
                };
                BookComponent.prototype.initBook = function (data) {
                    Book = new BookClass(this.config);
                    Book.init(data);
                };
                BookComponent.prototype.handleError = function (err) {
                    document.write('404 Not Found');
                };
                BookComponent = __decorate([
                    core_1.Component({
                        selector: 'dm-book',
                        templateUrl: 'app/book/book.component.html',
                        styleUrls: ['app/book/book.component.css'],
                        viewProviders: [http_1.HTTP_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [http_1.Http, router_1.RouteParams])
                ], BookComponent);
                return BookComponent;
            }());
            exports_1("BookComponent", BookComponent);
        }
    }
});
//# sourceMappingURL=book.component.js.map