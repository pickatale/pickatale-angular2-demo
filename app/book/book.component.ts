import { Component } from 'angular2/core';
import { RouteParams } from 'angular2/router';
import { Http, HTTP_PROVIDERS } from 'angular2/http';
import 'rxjs/Rx';

@Component({
    selector: 'dm-book',
    templateUrl: 'app/book/book.component.html',
    styleUrls: ['app/book/book.component.css'],
    viewProviders: [HTTP_PROVIDERS]
})
export class BookComponent {
    
    constructor(private _http: Http, private routeParams: RouteParams) {
        
    }
    
    version: string = '1.0';
    id: string = '305';
    lang: string = 'EN';
    
    config: any {
        version : this.version,
        id : this.id,
        lang : this.lang,
        baseUrl : 'http://cdn.pickatale.com/book_generated/',
        url : function(){
            return this.baseUrl + this.id + '_' + this.version + '/' + this.id + '_' + this.lang + '_' + this.version + '/book-data.json';
        },
        coverUrl : function(){
            return this.baseUrl + this.id + '_' + this.version + '/' + this.id + '_' + this.lang + '_' + this.version + '/';
        }
    };
    
    
    
    ngOnInit(): void {
       this._http.get(this.config.url())
            .map(res => res.json())
            .subscribe(
                data => this.initBook(data),
                err => this.handleError(err)
            );
    }
    
    initBook(data): void {
        Book= new BookClass(this.config);
        Book.init(data);
    }
    
    handleError(err): void {
        document.write('404 Not Found');
    }
}