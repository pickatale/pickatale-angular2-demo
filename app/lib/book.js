function BookClass(config) {
	var self = this;
	this.canvas = document.getElementById('bookCanvas');
	this.context = self.canvas.getContext('2d');
	this.mainDiv = document.getElementById('mainDiv');
	this.audio = document.getElementById('audioPage1');
	this.wordAudio = document.getElementById('wordAudio');
	this.orientation;
	this.orientationDiv = document.getElementById('orientationMsg');
	this.timeOuts = [];
	this.relation;
	this.baseUrl = config.baseUrl;
	this.coverUrl = config.coverUrl();
	var pageIndex = 0;
	var pageTo = 2;
	var isFirst = true;
	var started = false;
	var onPageEnd = false;
	
	self.audio.onended = function(){
		self.hideElement('pauseAudio');
		self.showElement('playAudio');

		// $('.words').css('color', 'rgb(' + self.fontColor || '255, 255, 255' + ')');

		self.revertColor();

		if(self.timeOuts.length > 0){
			for (var i = 0; i < self.timeOuts.length; i++) {
				self.timeOuts[i].clear();
			};
		}

		self.timeOuts = [];

		started = false;
		onPageEnd = true;
	};

	this.init = function(book) {
		self.book = book;
		for(var i = 0; i < pageTo; i++){
			self.book.pages[i].areaFixed = false;
		}
		self.orientation = self.book.cover.orientation;
		self.orientationInfo();
		self.cover(self.book);
	};

	this.createHtml = function(appendTo, type, id, elementClass){
		var element = document.createElement(type);
		element.setAttribute('id', id);
		element.setAttribute('class', '' || elementClass);
		document.getElementById(appendTo).appendChild(element);
		return element;
	};

	this.removeHtml = function(id){
		var element = document.getElementById(id);
		if(element !== null){
			element.parentNode.removeChild(element);
		}
	};

	this.removeHtmlClass = function(htmlClass){
		var element = document.getElementsByClassName(htmlClass);
		if(element.length){
			for(var i = 0; i < element.length; i++){
				element[i].parentNode.removeChild(element[i]);
			}
		}
	}

	this.hideElement = function(id){
		var element = document.getElementById(id);
		if(element !== null){
			element.style.visibility = 'hidden';
		}
	};

	this.showElement = function(id){
		var element = document.getElementById(id);
		if(element !== null){
			element.style.visibility = 'visible';
		}
	};

	this.hideClassElement = function(htmlClass){
		var element = document.getElementsByClassName(htmlClass);
		if(element.length){
			for(var i = 0; i < element.length; i++){
				element[i].style.visibility = 'hidden';
			}
		}
	};

	this.showClassElement = function(htmlClass){
		var element = document.getElementsByClassName(htmlClass);
		if(element.length){
			for(var i = 0; i < element.length; i++){
				element[i].style.visibility = 'visible';
			}
		}
	};

	this.revertColor = function () {
		var words = document.getElementsByClassName('words');
		if(words.length > 0){
			for(var i = 0; i < words.length; i++){
				words[i].style.color = 'rgb(' + self.fontColor + ')';
			}
		}
	};

	this.calculateCoords = function(x1, y1, x2, y2, relation){
		var a = parseInt(x1 * relation);
		var b = parseInt(y1 * relation);

		var c = parseInt(x2 * relation);
		var d = parseInt(y2 * relation);

		var width = c - a;
		var height = d - b;

		return {x1: a, y1: b, x2: c, y2: d, width: width, height: height}
	};

	this.flipPages = function(){
		self.pages(self.book.pages[pageIndex]);
	};

	this.backToFirst = function(){
		self.removeHtml('goBack');
		self.removeHtml('downloadIOS');
		self.removeHtml('downloadAndroid');
		pageIndex = 0;
		self.flipPages();
	};

	this.cover = function(data){
		self.removeHtmlClass('page-arrows');
		// self.winWidth = window.innerWidth;
		// self.winHeight = window.innerHeight;
		self.pagination = self.createHtml('mainDiv', 'span', 'pageNumber', 'pagination');

		var cover = new Image();
		cover.src = self.coverUrl + data.cover.imgUrl;

		cover.onload = function() {
			var imgWidth = cover.naturalWidth;
			var imgHeight = cover.naturalHeight;

			var size = self.resizeImage(window.innerWidth, window.innerHeight, imgWidth, imgHeight, null);
		
			self.mainDiv.style.width = size.width + 'px';
			self.mainDiv.style.height = size.height + 'px';

			self.canvas.height = size.height;
			self.canvas.width = size.width;

			self.context.drawImage(cover, 95, 80, imgWidth - 245, imgHeight - 190, 0, 0, size.width, size.height);

			// self.context.drawImage(cover, 0, 0, size.width, size.height);

			/* x, y, x1, y1 of cover page are calculated from static values because of json coordinates are not compatible with image coordinates */
		
			// var positions = self.calculateCoords(1312, 157, 1394, 392, size.relation)
			//
			// var autoPlay = self.createHtml('mainDiv', 'div', 'autoPlayBtn', 'auto-play-btn');
			//
			// autoPlay.style.left = positions.x1 + 'px';
			// autoPlay.style.top = positions.y1 + 'px';
			// autoPlay.style.width = positions.width + 'px';
			// autoPlay.style.height = positions.height + 'px';

			var readMyself = self.createHtml('mainDiv', 'img', 'readMyself', 'read-myself-btn');
			readMyself.src = 'app/assets/images/readmyself.png';

			readMyself.style.left = (size.width - 3) + 'px';
			readMyself.style.top = 10 + 'px';
			readMyself.style.width = parseInt(82 * size.relation) + 'px';
			readMyself.style.height = parseInt(235 * size.relation) + 'px';

			self.hideClassElement('sk-circle');

			self.addSideBackgrounds(size.width, size.height);
		};

	};

	this.addSideBackgrounds = function(width, height){
		var sides = parseInt(window.innerWidth - width);
		var sidePosition = parseInt(width + (sides / 2));

		self.leftBg = self.createHtml('leftBg', 'img', 'leftBgImg', 'side-bg');
		self.leftBg.src = 'app/assets/images/bg-left.png';

		self.rightBg = self.createHtml('rightBg', 'img', 'rightBgImg', 'side-bg');
		self.rightBg.src = 'app/assets/images/bg-right.png';
		self.sideBackgroundsPosition(width, height);
	};

	this.sideBackgroundsPosition = function (width, height) {
		var sides = parseInt(window.innerWidth - width);
		var sidePosition = parseInt(width + (sides / 2));

		self.leftBg.style.height = height + 'px';
		self.leftBg.style.right = (sidePosition - 3) + 'px';

		self.rightBg.style.height = height + 'px';
		self.rightBg.style.left = (sidePosition - 3) + 'px';
	}

	this.addPageArrows = function(width, height, relation, cb){
		isFirst = false;

		var leftArrow = self.createHtml('mainDiv', 'img', 'leftArrow', 'page-arrows');
		leftArrow.src = 'app/assets/images/button-prev.png';

		leftArrow.onload = function(){
			var rightArrow = self.createHtml('mainDiv', 'img', 'rightArrow', 'page-arrows');
			rightArrow.src = 'app/assets/images/button-next.png';
			
			if(self.orientation == 'portrait'){
				var pageEffect = self.createHtml('mainDiv', 'div', 'pageEffect', null)
			}else{
				var pageEffect = self.createHtml('mainDiv', 'img', 'pageEffect', null);
				pageEffect.src = 'app/assets/images/page-effect-mask.png';
			}

			var w = parseInt(leftArrow.width * relation);
			var h = parseInt(leftArrow.height * relation);

			var verticalAlign = parseInt((height / 2) - (h / 2));

			leftArrow.style.top = verticalAlign + 'px';
			leftArrow.style.width = w + 'px';
			leftArrow.style.height = h + 'px';

			rightArrow.style.top = verticalAlign + 'px';
			rightArrow.style.width = w + 'px';
			rightArrow.style.height = h + 'px';

			pageEffect.style.width = width + 'px';
			pageEffect.style.height = height + 'px';

			cb(true);
		}

	};

	this.setPage = function(fontSize, page){
		self.pagination.style.fontSize = fontSize + 'px';

		self.pagination.innerText = page;
	};

	this.audioButtons = function(width, relation){
		var x = parseInt(width - (61 * relation));
		var y = 0;


		var btnWidth = parseInt(53 * relation);
		var btnHeight = parseInt(61 * relation);

		var play = self.createHtml('mainDiv', 'img', 'playAudio', null);
		var pause = self.createHtml('mainDiv', 'img', 'pauseAudio', null);

			play.src = 'app/assets/images/button-play.png';
			play.style.top = y + 'px';
			play.style.left = x + 'px';
			play.style.width = btnWidth + 'px';
			play.style.height = btnHeight + 'px';

			pause.src = 'app/assets/images/button-pause.png';
			pause.style.top = y + 'px';
			pause.style.left = x + 'px';
			pause.style.width = btnWidth + 'px';
			pause.style.height = btnHeight + 'px';
			pause.style.visibility = 'hidden';

	};

	this.pages = function(data){
		self.showElement('playAudio');
		self.removeHtmlClass('word-container');
		self.showClassElement('sk-circle');
		self.audio.src = self.baseUrl + data.mp3Url;
		self.audio.load();

		var page = new Image();
		page.src = self.baseUrl + data.imgUrl;

		page.onload = function() {
			self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);
			var width = page.naturalWidth;
			var height = page.naturalHeight;

			var size = self.resizeImage(window.innerWidth, window.innerHeight, width, height, data.areas);
			self.relation = size.relation;


			self.showClassElement('page-arrows');
			self.showClassElement('pagination');
			self.showElement('pageEffect');

			if(pageIndex === 0){
				if(isFirst){

					self.audioButtons(size.width, size.relation)
					self.addPageArrows(size.width, size.height, size.relation, function(){
						self.hideElement('leftArrow');
					});


				}else{
					self.hideElement('leftArrow');
				}
					
			}else{
				self.showElement('leftArrow');
			}

			self.setPage(parseInt(data.texts[0].fontSize * size.relation) + 5, pageIndex + 2);
			
			self.mainDiv.style.width = size.width + 'px';
			self.mainDiv.style.height = size.height + 'px';

			self.canvas.height = size.height;
			self.canvas.width = size.width;
			self.text(data, size.relation);

			self.context.drawImage(page, 0, 0, size.width, size.height);

			self.sideBackgroundsPosition(size.width, size.height);
			self.hideClassElement('sk-circle');
		};
	};

	this.lastPage = function(){
		self.hideElement('pageEffect');
		self.hideElement('playAudio');
		self.hideElement('pauseAudio');
		self.hideClassElement('page-arrows');
		self.hideClassElement('word-container');
		self.hideClassElement('pagination');
		self.showClassElement('sk-circle');
			var lastPage = new Image();

			lastPage.src = 'app/assets/images/last-page2.png';

			lastPage.onload = function() {
				// self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);
				var imgWidth = lastPage.naturalWidth;
				var imgHeight = lastPage.naturalHeight;

				var size = self.resizeImage(window.innerWidth, window.innerHeight, imgWidth, imgHeight, null);

				self.context.drawImage(lastPage, 0, 0, size.width, size.height);
				

				/* x and y are calculated from static values because of json coordinates are not compatible with image coordinates */

				var readAgain = self.createHtml('mainDiv', 'div', 'readAgain', null);
				// var readAgainPos = self.calculateCoords(171, 342, 397, 560, size.relation); for 2048 x 1536
				var readAgainPos = self.calculateCoords(85, 171, 199, 280, size.relation);

				readAgain.style.top = readAgainPos.y1 + 'px';
				readAgain.style.left = readAgainPos.x1 + 'px';
				readAgain.style.width = readAgainPos.width + 'px';
				readAgain.style.height = readAgainPos.height + 'px';

				var back = self.createHtml('mainDiv', 'div', 'goBack', null);
				// var backPos = self.calculateCoords(1036, 441, 1212, 541, size.relation);
				var backPos = self.calculateCoords(518, 220, 606, 270, size.relation);

				back.style.top = backPos.y1 + 'px';
				back.style.left = backPos.x1 + 'px';
				back.style.width = backPos.width + 'px';
				back.style.height = backPos.height + 'px';

				var ios = self.createHtml('mainDiv', 'div', 'downloadIOS', null);
				// var iosPos = self.calculateCoords(1283, 958, 1832, 1065, size.relation);
				var iosPos = self.calculateCoords(641, 479, 916, 532, size.relation);

				ios.style.top = iosPos.y1 + 'px';
				ios.style.left = iosPos.x1 + 'px';
				ios.style.width = iosPos.width + 'px';
				ios.style.height = iosPos.height + 'px';

				var andorid = self.createHtml('mainDiv', 'div', 'downloadAndroid', null);
				// var andoridPos = self.calculateCoords(1290, 1191, 1928, 1309, size.relation);
				var andoridPos = self.calculateCoords(645, 595, 964, 654, size.relation);

				andorid.style.top = andoridPos.y1 + 'px';
				andorid.style.left = andoridPos.x1 + 'px';
				andorid.style.width = andoridPos.width + 'px';
				andorid.style.height = andoridPos.height + 'px';


				// self.sideBackgroundsPosition(size.width, size.height);
				self.hideClassElement('sk-circle');
			};
	};

	this.text = function(data, relation){
		for(var i = 0; i < data.texts.length; i++){
			var text = data.texts[i];
			var pos = text.pos.split(',');
			var contetDivs = '';
			var index = 0;

			self.fontSize = text.fontSize;
			self.fontColor = (text.fontColor == ',,' ? '0,0,0' : text.fontColor);

			for(var j = 0; j < text.timing.length; j++){
				var timing = text.timing[j];
				var content = '';
				if(timing.text != '[END]' && timing.text != '\\n'){
					content += timing.text;
					content += self.addSpace(timing.spaces_after);
					if(timing.time > 0){
						contetDivs += '<span class="words" index="' + j + '" class-index="' + index + '" id=page'+ (pageIndex + 1) + '_word_' + j + '>' + content + '</span>';
						// self.setText(timing.time, 'page'+ (pageIndex + 1) + '_word_' + j, text.fontColor);
						index++;
					}else{
						contetDivs += '<span class="words-symbols">' + content + '</span>';
					}
				}
			}

			var maxWidth = parseInt(text.width * relation);
			var lineHeight = 25 * relation;

			var x = parseInt(pos[0] * relation);
			var y = parseInt(pos[1] * relation);

			var textDiv = self.createHtml('mainDiv', 'div', 'page' + (pageIndex + 1), 'word-container');

			textDiv.innerHTML = contetDivs;

			textDiv.style.top = y + 'px';
			textDiv.style.left = x + 'px';
			textDiv.style.maxWidth = maxWidth + 'px';
			textDiv.style.fontSize = text.fontSize * relation + 'px';
			textDiv.style.color = 'rgb(' + text.fontColor + ')';

		}

	};

	this.playText = function (data, relation) {
		for(var i = 0; i < data.texts.length; i++){
			var text = data.texts[i];
			for(var j = 0; j < text.timing.length; j++){
				var timing = text.timing[j];
				if(timing.text != '[END]' && timing.text != '\\n'){
					if(timing.time > 0){
						self.setText(timing.time, 'page'+ (pageIndex + 1) + '_word_' + j, self.fontColor);

					}
				}
			}

		}
	};

	this.howToResume = function(){
		self.hideClassElement('sk-circle');
		if(started){
			self.resume();
		}else{
			self.revertColor();
			started = true;
			if(onPageEnd){
				// self.removeHtmlClass('word-container');
				onPageEnd = false;
				self.audio.currentTime = 0;

				var isAudioPlaying = true;
				self.audio.play();

				self.showClassElement('sk-circle');
				self.audio.ontimeupdate = function () {
					if(isAudioPlaying && this.currentTime > 0){
						self.hideClassElement('sk-circle');
						// self.text(self.book.pages[pageIndex], self.relation);
						self.playText(self.book.pages[pageIndex], self.relation);
						isAudioPlaying = false;
					}
				}
				
			}else{
				var isAudioPlaying = true;
				self.audio.play();
				self.showClassElement('sk-circle');

				self.audio.ontimeupdate = function () {
					if(isAudioPlaying && this.currentTime > 0){
						self.hideClassElement('sk-circle');
						// self.text(self.book.pages[pageIndex], self.relation);
						isAudioPlaying = false;
						self.playText(self.book.pages[pageIndex], self.relation);
					}
				}
			}
		}
	};

	this.pause = function(){
		if(self.timeOuts.length > 0){
			for (var i = 0; i < self.timeOuts.length; i++) {
				self.timeOuts[i].pause();
			};
			self.audio.pause();
		}
	};

	this.resume = function(){
		if(self.timeOuts.length > 0){
			for (var i = 0; i < self.timeOuts.length; i++) {
				self.timeOuts[i].resume();
			};
			self.audio.play();

		}
	};

	this.onBlur = function () {
		if(started){
			self.audio.pause();
			self.audio.currentTime = 0;

			self.hideElement('pauseAudio');
			self.showElement('playAudio');

			if(self.timeOuts.length > 0){
				for (var i = 0; i < self.timeOuts.length; i++) {
					self.timeOuts[i].clear();
				};
			}

			self.timeOuts = [];

			started = false;
			onPageEnd = true;
		}
	};

	this.orientationInfo = function () {
		var orientation = self.orientation;
		if(window.orientation == 90 || window.orientation == -90){
			//landscape
			if(orientation == 'landscape'){
				self.hideOrientation();
			}else{
				self.showOrientation(orientation);	
			}
		}else{
			if(window.orientation == undefined){
				if(screen.orientation.angle == 90 || screen.orientation.angle == 270){
					// self.hideOrientation();
					if(orientation == 'landscape'){
						self.hideOrientation();
					}else{
						self.showOrientation(orientation);	
					}
				}else if(!isMobile()){
					self.hideOrientation();
				}else{
					// self.showOrientation();
					if(orientation == 'landscape'){
						self.hideOrientation();
					}else{
						self.showOrientation(orientation);	
					}
				}
			}else{
				// self.showOrientation();
				if(orientation == 'landscape'){
					self.hideOrientation();
				}else{
					self.showOrientation(orientation);	
				}
			}
		}
	};

	this.showOrientation = function (orientation) {
		self.orientationDiv.innerText = 'Please rotate your device to ' + orientation + ' mode';
		self.showElement("orientationMsg");
	};

	this.hideOrientation = function () {
		self.hideElement('orientationMsg');
	};

	this.orientationChange = function () {
		if(!orientationFixed){
			self.showClassElement('sk-circle');
			location.reload();
		}

		orientationFixed = true;
		self.audio.pause();
		self.hideElement('pauseAudio');
		self.showElement('playAudio');

		if(self.timeOuts.length > 0){
			for (var i = 0; i < self.timeOuts.length; i++) {
				self.timeOuts[i].clear();
			};
		}

		self.timeOuts = [];

		started = false;
		onPageEnd = true;
	};

	this.onPageFlip = function(cb){
		started = false;
		onPageEnd = false;
		self.hideElement('pauseAudio');
		self.showElement('playAudio');
		self.audio.pause();
		self.audio.currentTime = 0;
		self.audio.removeAttribute('src');


		if(self.timeOuts.length > 0){
			for (var i = 0; i < self.timeOuts.length; i++) {
				self.timeOuts[i].clear();
			};
		}

		self.timeOuts = [];
		cb();
	};

	this.nextPage = function(){
		self.onPageFlip(function(){
			pageIndex++;
			if(pageIndex <= pageTo){
				self.flipPages();
			}else{
				self.lastPage();
			}
		});
	};

	this.previousPage = function(){
		self.onPageFlip(function(){
			pageIndex--;
			self.flipPages();
		});
	};

	this.setText = function(time, id, defaultColor){
		if(time > 0){
			self.timeOuts.push(new Timer(function() {
				self.setColor('words', id, defaultColor);
			}, time));

		}
	};

	this.setColor = function(className, id, color){
		var element = document.getElementById(id);
		var elements = document.getElementsByClassName(className);

		var index = element.getAttribute('class-index');

		if(index > 0){
			elements[index - 1].style.color = 'rgb(' + color + ')';
		}
		elements[index].style.color = 'red';
	};

	this.addSpace = function(nr){
		var space = '';
		if(nr > 0){
			for(var i = 0; i < nr; i++){
				space += ' ';
			}
		}
		return space;
	};

	this.resizeImage = function(orgWidth, orgHeight, imgWidth, imgHeight, areas){
		var aspectRatio = imgWidth / imgHeight;
		var newWidth, newHeight, widthRelation, heightRelation;
		var CoordinatesArray = [];
		
		if(self.orientation == 'portrait'){
			newWidth = orgWidth - 120;
			newHeight = parseInt(newWidth / aspectRatio);
		}else{
			newHeight = orgHeight;
			newWidth = parseInt(newHeight * aspectRatio);
		}
		
		

		// if(orgWidth > orgHeight){
			// newHeight = orgHeight;
			// newWidth = parseInt(newHeight * aspectRatio);
		// 	if(newWidth > orgWidth){
		// 		newWidth = orgWidth;
		// 		newHeight = parseInt(newWidth / aspectRatio);
		// 	}
		// }else{
		// 	newWidth = orgWidth;
		// 	newHeight = parseInt(newWidth / aspectRatio);
		// 	if(newHeight > orgHeight){
		// 		newHeight = orgHeight;
		// 		newWidth = parseInt(newHeight * aspectRatio);
		// 	}
		// }


		widthRelation = newWidth / imgWidth;
		heightRelation = newHeight / imgHeight;

		if(areas !== null){
			if(!self.book.pages[pageIndex].areaFixed){
				self.book.pages[pageIndex].areaFixed = true;
				for(var i = 0; i < areas.length; i++){
					var coordinates = areas[i].coordinates;
					var tmpCoordinates = [];
					var tmpAreas = areas[i];
					for(var j = 0; j < coordinates.length; j++){
						var x = parseInt(widthRelation * coordinates[j].x);
						var y = parseInt(heightRelation * coordinates[j].y);

						tmpCoordinates.push({x: x, y: y});

					}
					tmpAreas.coordinates = tmpCoordinates;
					CoordinatesArray.push(tmpAreas);
				}
			}

		}


		return {height: newHeight, width: newWidth, areas: CoordinatesArray, relation: widthRelation};
	};


	this.isInside = function(nvert, vertx, verty, mx, my){
		/*	nvert - Number of vertices in the polygon. Whether to repeat the first vertex at the end is discussed below.
			vertx, verty - Arrays containing the x- and y-coordinates of the polygon's vertices.
			mx, my - X- and y-coordinate of the test point. */

		var i, j, isInside = false;
		for( i = 0, j = nvert-1; i < nvert; j = i++ ) {
			if( ( ( verty[i] > my ) != ( verty[j] > my ) ) &&
				( mx < ( vertx[j] - vertx[i] ) * ( my - verty[i] ) / ( verty[j] - verty[i] ) + vertx[i] ) ) {
					isInside = !isInside;
			}
		}
		return isInside;

	};

	this.convertAreas = function(data){
		var arrayX = [];
		var arrayY = [];

		for(var i = 0; i < data.length; i++){
			arrayX.push(data[i].x);
			arrayY.push(data[i].y);
		}

		return { x: arrayX, y: arrayY };
	};

	this.element = function(x, y, data){
		for(var i = data.length - 1; i >= 0; i--) {
			var arrays = self.convertAreas(data[i].coordinates);
			var isInside = self.isInside(data[i].coordinates.length, arrays.x, arrays.y, x, y);
			if(isInside){
				return data[i];
			}
		}
	}

	this.checkArea = function(x, y){
		var data = self.book.pages[pageIndex].areas;
		var element = self.element(x, y, data);

		if(element.mp3Url){
			self.wordAudio.src = self.baseUrl + element.mp3Url;
			self.wordAudio.play();
			setTimeout(function(){
				self.showWord(element.word, x, y);
			}, 100);
		}
		
	};

	this.showWord = function(word, x, y){
		var wordElement = self.createHtml('mainDiv', 'span', '', 'word-elements');
		wordElement.style.display = 'none';
		wordElement.style.left = x + 'px';
		wordElement.style.top = y + 'px';
		wordElement.style.fontSize = (self.fontSize || 28) * self.relation + 'px';
		wordElement.innerText = word;

		var rotation = parseInt(Math.random() * (45 - (-45)) + (-45));
		wordElement.style.transform = 'rotate(' + rotation + 'deg)';
		wordElement.style.webkitTransform = 'rotate(' + rotation + 'deg)';

		$(wordElement).fadeIn( 'fast', function(){
			setTimeout(function(){
				$(wordElement).fadeOut('fast', function(){
					$(wordElement).remove();
				});
			}, 150)
		});

	};

	this.playClickedText = function(index, elementId){
		var data = self.book.pages[pageIndex].texts[0].timing[index];
		var element = document.getElementById(elementId);
		
		if(data.mp3Url){
			self.wordAudio.src = self.baseUrl + data.mp3Url;
			self.wordAudio.load();
			self.wordAudio.play();
			setTimeout(function(){
				element.style.color = 'red';
			}, 100)
			setTimeout(function(){
				element.style.color = 'rgb(' + ( self.fontColor || '255, 255, 255') + ')';
			}, 300)
		}
	}

}