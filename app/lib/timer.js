// (function(){
    function Timer(callback, delay) {
        var timerId, start, remaining = delay;

        this.pause = function() {
            window.clearTimeout(timerId);
            remaining -= new Date() - start;
        };

        this.resume = function() {
            start = new Date();
            window.clearTimeout(timerId);
            timerId = window.setTimeout(callback, remaining);
        };

        this.clear = function() {
            window.clearTimeout(timerId);
        }

        this.resume();
    }

// var module = angular.module("pickatale-demo");
//     module.factory("timer", timer);
// }());