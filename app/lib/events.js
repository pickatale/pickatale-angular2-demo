var clickHandler = ('ontouchstart' in document.documentElement ? "touchstart" : "click");

$(document).on('touchmove', 'div, img, span', function(e){
	return false;
});

$(document).on(clickHandler, '#autoPlayBtn', function(){
	Book.flipPages();
	$(this).remove();
});

$(document).on(clickHandler, '#readMyself', function(){
	Book.flipPages();
	$(this).remove();
});

$(document).on(clickHandler, '#readAgain', function(){
	Book.backToFirst();
	$(this).remove();
});

$(document).on(clickHandler, '#goBack', function(){
	// code here
});

$(document).on(clickHandler, '#downloadIOS', function(){
	window.location = 'https://itunes.apple.com/cn/app/pickatale-200+-interactive/id669433655?mt=8';
});

$(document).on(clickHandler, '#downloadAndroid', function(){
	window.location = 'https://apps.wandoujia.com/apps/com.WisdomEdition.Pickatale.Bookshelf?pos=w%2Fapps%2Fcategories%2F%E4%B8%BD%E4%BA%BA%E6%AF%8D%E5%A9%B4%2FsuperiorFirst';
});

$(document).on(clickHandler, '#playAudio', function(){
	Book.howToResume();
	
	Book.hideElement('playAudio');
	Book.showElement('pauseAudio');
});

$(document).on(clickHandler, '#pauseAudio', function(){
	Book.pause();
	Book.hideElement('pauseAudio');
	Book.showElement('playAudio');
});

$(document).on(clickHandler, '#leftArrow', function(){
	Book.previousPage();
});

$(document).on(clickHandler, '#rightArrow', function(){
	Book.nextPage();
});

$(document).on(clickHandler, '#pageEffect', function(event){
	var offset = $(this).offset();
	if(clickHandler === 'touchstart'){
	    var x = parseInt(event.originalEvent.touches[0].pageX - offset.left);
	    var y = parseInt(event.originalEvent.touches[0].pageY - offset.top);
	}else{
	    var x = parseInt(event.clientX - offset.left);
	    var y = parseInt(event.clientY - offset.top);
	}
    Book.checkArea(x, y);

});

$(document).on(clickHandler, '.words', function(){
	Book.playClickedText($(this).attr('index'), $(this).attr('id'));
});

function isMobile(){
	return navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|IEMobile)/);
}


