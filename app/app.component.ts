import { Component } from 'angular2/core';
import { ROUTER_PROVIDERS, RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router';
import { BookComponent } from './book/book.component';
import { DynamicBookComponent } from './book/dynamic-book.component';

@Component({
    selector: 'dm-index',
    template: `<router-outlet></router-outlet>`,
    directives: [ROUTER_DIRECTIVES],
    providers: [ROUTER_PROVIDERS]
})
@RouteConfig([
    { path: "/book", name: "Book", component: BookComponent, useAsDefault: true },
    { path: "/book/:lang/:id", name: "DynamicBook", component: DynamicBookComponent },
    { path: '/**', redirectTo: ['Book'] }
])
export class AppComponent {
    
}